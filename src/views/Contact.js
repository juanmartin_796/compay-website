import React, { useState } from 'react';
// import sections
import Form from '../components/sections/Form';
import Modal from '../components/elements/Modal';

const Contact = () => {
  const [showModal, setShowModal] = useState(false)

  return (
    <>
      <div className="container mt-5 pt-5 reveal-from-bottom">

        <div className="row">
          <div className="col-sm">
            <h2 className='mt-0'>Let's talk about your needs!</h2>
            <p>
              We’d love to be your partner in this digital world, finding creative and useful solutions for you. Fill in the form to the right so we can reach you and have a conversation.
            </p>
          </div>
          <Modal handleClose={()=> setShowModal(false)} show={showModal} closeHidden={true}>
            <p>
              Thanks!
            </p>
            <p>
              Your message was sent! I will contact to you as soon as posible.
            </p>
            <div className="modal-footer mb-0 pb-0">
              <button type="button" className="btn btn-primary" onClick={() =>setShowModal(false) }>Ok</button>
            </div>
          </Modal>
          <div className="col-sm">
            <Form onSubmitted={()=> setShowModal(true)}/>
          </div>
        </div>
      </div>

    </>
  );
}

export default Contact;